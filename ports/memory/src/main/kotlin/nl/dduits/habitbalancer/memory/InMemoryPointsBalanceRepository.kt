package nl.dduits.habitbalancer.memory

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.datetime.Instant
import nl.dduits.habitbalancer.core.BalanceChange
import nl.dduits.habitbalancer.core.PointsBalanceRepository

class InMemoryPointsBalanceRepository : PointsBalanceRepository {

    private var idCounter = 0
    private data class BalanceChange(val id: Int, val change: Int, val instant: Instant)
    private var balance = MutableStateFlow<List<BalanceChange>>(emptyList())

    override suspend fun getBalance(): Int = balance.value.sumOf { it.change }

    override fun getObservableBalance(): Flow<Int> = balance
        .map { list -> list.sumOf { it.change } }

    override suspend fun changeBalance(points: Int, instant: Instant) {
        require(points != 0)
        balance.value += BalanceChange(idCounter++, points, instant)
    }

    override suspend fun removeNewestBalanceChange() {
        val listSnapshot = balance.value

        val newestBalanceChange = listSnapshot.getNewestBalanceChange()
        if (newestBalanceChange != null) {
            val newList = listSnapshot.toMutableList()
            newList.remove(newestBalanceChange)
            balance.value = newList
        } else {
            throw IllegalStateException("Newest balance change doesn't exist.")
        }
    }

    override suspend fun getNewestBalanceChange(): nl.dduits.habitbalancer.core.BalanceChange? =
        balance.value.getNewestBalanceChange()?.let { internalBalanceChange ->
            BalanceChange(
                change = internalBalanceChange.change,
                instant = internalBalanceChange.instant
            )
        }

    private fun List<BalanceChange>.getNewestBalanceChange() = maxByOrNull { it.instant }
}
