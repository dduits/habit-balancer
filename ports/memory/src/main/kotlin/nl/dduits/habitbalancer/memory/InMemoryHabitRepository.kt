package nl.dduits.habitbalancer.memory

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.firstOrNull
import nl.dduits.habitbalancer.core.Habit
import nl.dduits.habitbalancer.core.HabitRepository

class InMemoryHabitRepository : HabitRepository {

    private val habits: MutableStateFlow<List<Habit>> = MutableStateFlow(emptyList())

    override suspend fun createHabit(habit: Habit) {
        val current = habits.value
        habits.value = current + habit
    }

    override fun getHabits(): Flow<List<Habit>> = habits

    override suspend fun getHabitById(id: Long): Habit? =
        (habits.firstOrNull() ?: emptyList())
            .firstOrNull { it.id == id }
}
