package nl.dduits.habitbalancer.memory

import nl.dduits.habitbalancer.core.HabitCooldown
import nl.dduits.habitbalancer.core.HabitCooldownRepository

class InMemoryHabitCooldownRepository : HabitCooldownRepository {

    private val habitCooldowns: MutableMap<Long, HabitCooldown> = mutableMapOf()

    override suspend fun registerHabitCooldown(habitCooldown: HabitCooldown) {
        habitCooldowns[habitCooldown.habitId] = habitCooldown
    }

    override suspend fun getHabitCooldownById(id: Long): HabitCooldown? =
        habitCooldowns[id]

    override suspend fun getHabitCooldownsWithIds(ids: List<Long>): List<HabitCooldown> =
        habitCooldowns
            .filter { ids.contains(it.key) }
            .values
            .toList()
}
