package nl.dduits.habitbalancer.memory

import io.kotest.core.spec.style.FunSpec
import nl.dduits.habitbalancer.core.repo.habitCooldownRepositoryTests

internal class InMemoryHabitCooldownRepositoryTest : FunSpec({

    val repository = InMemoryHabitCooldownRepository()
    val habitRepository = InMemoryHabitRepository()

    include(habitCooldownRepositoryTests(habitRepository, repository))
})
