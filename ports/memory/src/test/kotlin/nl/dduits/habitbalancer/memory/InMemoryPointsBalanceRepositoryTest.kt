package nl.dduits.habitbalancer.memory

import io.kotest.core.spec.style.FunSpec
import nl.dduits.habitbalancer.core.repo.pointsBalanceRepositoryTests

internal class InMemoryPointsBalanceRepositoryTest : FunSpec({
    val inMemoryPointsBalanceRepository = InMemoryPointsBalanceRepository()
    include(pointsBalanceRepositoryTests(inMemoryPointsBalanceRepository))
})
