package nl.dduits.habitbalancer.memory

import io.kotest.core.spec.style.FunSpec
import nl.dduits.habitbalancer.core.repo.habitRepositoryTests

internal class InMemoryHabitRepositoryTest : FunSpec({
    val repository = InMemoryHabitRepository()
    include(habitRepositoryTests(repository))
})
