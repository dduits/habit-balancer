package nl.dduits.habitbalancer.room

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import nl.dduits.habitbalancer.core.Habit
import nl.dduits.habitbalancer.core.HabitRepository
import kotlin.time.Duration.Companion.seconds

class RoomHabitRepository(
    private val dao: HabitDao
) : HabitRepository {

    override suspend fun createHabit(habit: Habit) {
        val entity = habit.toEntity()
        dao.insertHabit(entity)
    }

    override fun getHabits(): Flow<List<Habit>> =
        dao.queryFlowAllHabits().map { list -> list.map { it.toHabit() } }

    override suspend fun getHabitById(id: Long): Habit? = dao.queryHabitById(id)?.toHabit()
}

private fun Habit.toEntity(): HabitEntity {
    val minDurationForPointsInSeconds = when (val type = pointsPerHabitType) {
        Habit.PointsPerHabit.Event -> null
        is Habit.PointsPerHabit.Overtime -> type.duration.inWholeSeconds
    }

    return HabitEntity(
        id = id,
        displayText = displayText,
        pointsPerHabit = pointsPerHabit,
        minDurationForPointsInSeconds = minDurationForPointsInSeconds,
        cooldownInSeconds = pointsModifier?.resetPeriod?.inWholeSeconds,
        cooldownMultiplier = pointsModifier?.multiplier
    )
}

private fun HabitEntity.toHabit(): Habit {
    val type = when (minDurationForPointsInSeconds) {
        null -> Habit.PointsPerHabit.Event
        else -> {
            val duration = minDurationForPointsInSeconds.seconds
            Habit.PointsPerHabit.Overtime(duration)
        }
    }

    val pointsModifier = if (cooldownMultiplier != null && cooldownInSeconds != null) {
        Habit.PointsModifier(cooldownMultiplier, cooldownInSeconds.seconds)
    } else {
        null
    }

    return Habit(
        id = id,
        displayText = displayText,
        pointsPerHabit = pointsPerHabit,
        pointsPerHabitType = type,
        pointsModifier = pointsModifier
    )
}
