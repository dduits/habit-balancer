package nl.dduits.habitbalancer.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    version = 1,
    exportSchema = false,
    entities = [HabitBalanceChange::class, HabitEntity::class, HabitCooldownEntity::class]
)
abstract class HabitBalancerRoomDatabase : RoomDatabase() {
    abstract fun getHabitBalanceDao(): HabitBalanceDao
    abstract fun getHabitDao(): HabitDao
    abstract fun getHabitCooldownDao(): HabitCooldownDao
}
