package nl.dduits.habitbalancer.room

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "habit_cooldown",
    foreignKeys = [
        ForeignKey(
            entity = HabitEntity::class,
            parentColumns = ["id"],
            childColumns = ["habitId"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class HabitCooldownEntity(
    @PrimaryKey val habitId: Long = 0,
    val cooldownViolations: Int,
    val instant: Long
)
