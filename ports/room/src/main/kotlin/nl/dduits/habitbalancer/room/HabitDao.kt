package nl.dduits.habitbalancer.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface HabitDao {

    @Insert
    suspend fun insertHabit(entity: HabitEntity)

    @Query("SELECT * FROM habit")
    fun queryFlowAllHabits(): Flow<List<HabitEntity>>

    @Query("SELECT * FROM habit WHERE id = :id")
    suspend fun queryHabitById(id: Long): HabitEntity?
}
