package nl.dduits.habitbalancer.room

import android.content.Context
import androidx.room.Room

class HabitBalancerRoomDatabaseBuilderImpl(private val context: Context) :
    HabitBalancerRoomDatabaseBuilder {
    override fun create() =
        Room.databaseBuilder(context, HabitBalancerRoomDatabase::class.java, "habit_balancer_db")
            .build()
}
