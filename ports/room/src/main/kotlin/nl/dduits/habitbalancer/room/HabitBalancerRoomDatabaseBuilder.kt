package nl.dduits.habitbalancer.room

fun interface HabitBalancerRoomDatabaseBuilder {
    fun create(): HabitBalancerRoomDatabase
}
