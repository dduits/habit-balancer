package nl.dduits.habitbalancer.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface HabitBalanceDao {

    @Insert
    fun insertBalanceChange(balanceChange: HabitBalanceChange)

    @Query("SELECT SUM(pointsChange) FROM balance_change")
    suspend fun getBalanceSum(): Int?

    @Query("SELECT SUM(pointsChange) FROM balance_change")
    fun getObservableBalanceSum(): Flow<Int?>

    @Query("SELECT * FROM balance_change ORDER BY id DESC LIMIT 1")
    suspend fun getNewestBalanceChange(): HabitBalanceChange?

    @Delete
    suspend fun delete(change: HabitBalanceChange)
}
