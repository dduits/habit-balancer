package nl.dduits.habitbalancer.room

import kotlinx.datetime.Instant
import nl.dduits.habitbalancer.core.HabitCooldown
import nl.dduits.habitbalancer.core.HabitCooldownRepository

class RoomHabitCooldownRepository(
    private val dao: HabitCooldownDao
) : HabitCooldownRepository {

    override suspend fun registerHabitCooldown(habitCooldown: HabitCooldown) {
        dao.insertAndReplaceCooldown(habitCooldown.toEntity())
    }

    override suspend fun getHabitCooldownById(id: Long): HabitCooldown? =
        dao.queryHabitCooldownById(id)?.toHabitCooldown()

    override suspend fun getHabitCooldownsWithIds(ids: List<Long>): List<HabitCooldown> =
        dao.queryHabitCooldownsByIds(ids)
            .map(HabitCooldownEntity::toHabitCooldown)

}

private fun HabitCooldown.toEntity() = HabitCooldownEntity(
    habitId = habitId,
    cooldownViolations = violations,
    instant = violationInstant.epochSeconds
)

private fun HabitCooldownEntity.toHabitCooldown() = HabitCooldown(
    habitId = habitId,
    violations = cooldownViolations,
    violationInstant = Instant.fromEpochSeconds(instant)
)
