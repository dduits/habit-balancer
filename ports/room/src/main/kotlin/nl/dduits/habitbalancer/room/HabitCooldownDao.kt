package nl.dduits.habitbalancer.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface HabitCooldownDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAndReplaceCooldown(cooldown: HabitCooldownEntity)

    @Query("SELECT * FROM habit_cooldown WHERE habitId = :id")
    fun queryHabitCooldownById(id: Long): HabitCooldownEntity?

    @Query("SELECT * FROM habit_cooldown WHERE habitId IN (:ids)")
    fun queryHabitCooldownsByIds(ids: List<Long>): List<HabitCooldownEntity>
}
