package nl.dduits.habitbalancer.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "balance_change")
data class HabitBalanceChange(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val pointsChange: Int,
    val epochMilliseconds: Long,
)
