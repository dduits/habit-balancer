package nl.dduits.habitbalancer.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "habit")
data class HabitEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val displayText: String,
    val pointsPerHabit: Int,
    val minDurationForPointsInSeconds: Long?,
    val cooldownInSeconds: Long?,
    val cooldownMultiplier: Double?
)
