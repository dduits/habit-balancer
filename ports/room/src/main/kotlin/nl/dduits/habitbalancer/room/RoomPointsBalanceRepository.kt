package nl.dduits.habitbalancer.room

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.datetime.Instant
import nl.dduits.habitbalancer.core.BalanceChange
import nl.dduits.habitbalancer.core.PointsBalanceRepository

class RoomPointsBalanceRepository(
    private val dao: HabitBalanceDao
) : PointsBalanceRepository {

    override suspend fun getBalance(): Int {
        return dao.getBalanceSum() ?: 0
    }

    override fun getObservableBalance(): Flow<Int> {
        return dao.getObservableBalanceSum()
            .mapNotNull { it ?: 0 }
    }

    override suspend fun changeBalance(points: Int, instant: Instant) {
        require(points != 0)
        dao.insertBalanceChange(
            HabitBalanceChange(
                pointsChange = points,
                epochMilliseconds = instant.toEpochMilliseconds()
            )
        )
    }

    override suspend fun getNewestBalanceChange() =
        dao.getNewestBalanceChange()
            ?.toBalanceChange()

    override suspend fun removeNewestBalanceChange() {
        val change = dao.getNewestBalanceChange()

        if (change != null) {
            dao.delete(change)
        } else {
            throw IllegalStateException("Balance change was not found.")
        }
    }
}

private fun HabitBalanceChange.toBalanceChange() = BalanceChange(
    change = pointsChange,
    instant = Instant.fromEpochMilliseconds(epochMilliseconds)
)

