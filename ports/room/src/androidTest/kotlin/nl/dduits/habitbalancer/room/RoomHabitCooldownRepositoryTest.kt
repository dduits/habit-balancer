package nl.dduits.habitbalancer.room

import io.kotest.core.spec.style.FunSpec
import io.kotest.runner.junit4.KotestTestRunner
import nl.dduits.habitbalancer.core.repo.habitCooldownRepositoryTests
import org.junit.runner.RunWith

@RunWith(KotestTestRunner::class)
internal class RoomHabitCooldownRepositoryTest : FunSpec({

    val database = instrumentedDatabaseBuilder().create()
    val habitDao = database.getHabitDao()
    val habitCooldownDao = database.getHabitCooldownDao()

    val habitRepository = RoomHabitRepository(habitDao)
    val habitCooldownRepository = RoomHabitCooldownRepository(habitCooldownDao)

    include(habitCooldownRepositoryTests(habitRepository, habitCooldownRepository))
})
