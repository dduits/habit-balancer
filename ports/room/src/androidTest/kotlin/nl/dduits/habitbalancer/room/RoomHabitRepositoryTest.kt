package nl.dduits.habitbalancer.room

import io.kotest.core.spec.style.FunSpec
import io.kotest.runner.junit4.KotestTestRunner
import nl.dduits.habitbalancer.core.repo.habitRepositoryTests
import org.junit.runner.RunWith

@RunWith(KotestTestRunner::class)
internal class RoomHabitRepositoryTest : FunSpec({

    val database = instrumentedDatabaseBuilder().create()
    val dao = database.getHabitDao()
    val repository = RoomHabitRepository(dao)

    include(habitRepositoryTests(repository))
})
