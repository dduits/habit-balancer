package nl.dduits.habitbalancer.room

import io.kotest.core.spec.style.FunSpec
import io.kotest.runner.junit4.KotestTestRunner
import nl.dduits.habitbalancer.core.repo.pointsBalanceRepositoryTests
import org.junit.runner.RunWith

@RunWith(KotestTestRunner::class)
internal class RoomPointBalanceRepositoryTest : FunSpec({

    val database = instrumentedDatabaseBuilder().create()
    val dao = database.getHabitBalanceDao()
    val pointsBalanceRepository = RoomPointsBalanceRepository(dao)

    include(pointsBalanceRepositoryTests(pointsBalanceRepository))
})
