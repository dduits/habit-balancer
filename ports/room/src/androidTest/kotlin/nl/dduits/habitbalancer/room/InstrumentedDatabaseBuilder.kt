package nl.dduits.habitbalancer.room

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider

internal fun instrumentedDatabaseBuilder() = HabitBalancerRoomDatabaseBuilder {
    Room
        .inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            HabitBalancerRoomDatabase::class.java
        )
        .build()
}
