package nl.dduits.habitbalancer.ui.list

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.LocalContentColor
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.node.modifierElementOf
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import nl.dduits.habitbalancer.ui.R
import nl.dduits.habitbalancer.ui.common.HabitBalancerTheme

@Composable
fun CreateHabitDialog(
    modifier: Modifier = Modifier,
    submit: (CreateHabit) -> Unit,
    onDismiss: () -> Unit
) {

    var name by remember { mutableStateOf("") }
    var points by remember { mutableStateOf(50) }
    var time by remember { mutableStateOf(30) }
    var pointsModifier by remember { mutableStateOf(1.0) }
    var modifierReset by remember { mutableStateOf(120) }

    Dialog(
        onDismissRequest = onDismiss
    ) {
        Box(
            modifier = modifier
                .fillMaxWidth()
                .background(color = MaterialTheme.colors.surface, shape = RoundedCornerShape(16.dp))
                .padding(16.dp)
        ) {
            Column {
                Text(
                    text = stringResource(id = R.string.create_habit_heading),
                    style = MaterialTheme.typography.h6
                )

                Spacer(modifier = Modifier.padding(8.dp))

                val onNameChange: (String) -> Unit = remember {
                    { name = it }
                }

                val onPointsChange: (String) -> Unit = remember {
                    { points = it.toIntOrNull() ?: 0 }
                }

                val onTimeChange: (String) -> Unit = remember {
                    { time = it.toIntOrNull() ?: 0 }
                }

                val onPointsModifierChange: (String) -> Unit = remember {
                    { pointsModifier = it.toDoubleOrNull() ?: 0.0 }
                }

                val onModifierResetChange: (String) -> Unit = remember {
                    { modifierReset = it.toIntOrNull() ?: 0 }
                }

                TextFields(
                    name = name,
                    points = points,
                    time = time,
                    pointsModifier = pointsModifier,
                    modifierReset = modifierReset,
                    onNameChange = onNameChange,
                    onPointsChange = onPointsChange,
                    onTimeChange = onTimeChange,
                    onPointsModifierChange = onPointsModifierChange,
                    onModifierResetChange = onModifierResetChange
                )

                Spacer(modifier = Modifier.padding(8.dp))

                val onCreate = remember {
                    { submit(CreateHabit(name, points, time, pointsModifier, modifierReset)) }
                }

                Buttons(
                    modifier = Modifier.fillMaxWidth(),
                    onCreate = onCreate,
                    onClose = onDismiss
                )
            }
        }
    }
}

@Composable
private fun TextFields(
    modifier: Modifier = Modifier,
    name: String,
    points: Int,
    time: Int,
    pointsModifier: Double,
    modifierReset: Int,
    onNameChange: (String) -> Unit,
    onPointsChange: (String) -> Unit,
    onTimeChange: (String) -> Unit,
    onPointsModifierChange: (String) -> Unit,
    onModifierResetChange: (String) -> Unit
) {
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        val colors = TextFieldDefaults.textFieldColors(
            cursorColor = LocalContentColor.current,
            focusedLabelColor = LocalContentColor.current,
            focusedIndicatorColor = LocalContentColor.current
        )

        TextField(
            value = name,
            label = { Text(text = stringResource(id = R.string.create_habit_display_name_field_title)) },
            placeholder = { Text(text = stringResource(id = R.string.create_habit_display_name_field_placeholder)) },
            onValueChange = onNameChange,
            colors = colors
        )

        TextField(
            value = points.toString(),
            label = { Text(text = stringResource(id = R.string.create_habit_points_field_title)) },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
            onValueChange = onPointsChange,
            colors = colors
        )

        TextField(
            value = time.toString(),
            label = { Text(text = stringResource(id = R.string.create_habit_time_field_title)) },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
            onValueChange = onTimeChange,
            colors = colors
        )

        Row {

            TextField(
                modifier = Modifier.weight(0.33f),
                value = pointsModifier.toString(),
                label = { Text(text = stringResource(id = R.string.create_habit_points_modifier_field_title)) },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                onValueChange = onPointsModifierChange,
                colors = colors
            )
            
            Spacer(modifier = Modifier.width(8.dp))

            TextField(
                modifier = Modifier.weight(0.66f),
                value = modifierReset.toString(),
                label = { Text(text = stringResource(id = R.string.create_habit_points_modifier_reset_field_title)) },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                onValueChange = onModifierResetChange,
                colors = colors
            )
        }
    }
}

@Composable
private fun Buttons(
    modifier: Modifier = Modifier,
    onCreate: () -> Unit,
    onClose: () -> Unit,
) {

    val textButtonColors = ButtonDefaults.textButtonColors(
        contentColor = MaterialTheme.colors.onSurface
    )

    Row(
        modifier = modifier,
        horizontalArrangement = Arrangement.End
    ) {
        TextButton(
            colors = textButtonColors,
            onClick = onClose
        ) {
            Text(
                text = stringResource(id = R.string.create_habit_dismiss_button),
                style = MaterialTheme.typography.button
            )
        }

        Spacer(modifier = Modifier.width(16.dp))

        TextButton(
            colors = textButtonColors,
            onClick = onCreate
        ) {
            Log.wtf("color", "${LocalContentColor.current.value}")
            Text(
                text = stringResource(id = R.string.create_habit_submit_button),
                style = MaterialTheme.typography.button
            )
        }
    }
}

@Preview
@Composable
fun CreateHabitDialogPreview() {
    HabitBalancerTheme {
        CreateHabitDialog(
            submit = {},
            onDismiss = {}
        )
    }
}
