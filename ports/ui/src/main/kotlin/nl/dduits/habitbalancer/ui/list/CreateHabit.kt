package nl.dduits.habitbalancer.ui.list

data class CreateHabit(
    val displayName: String,
    val points: Int,
    val time: Int,
    val pointsModifier: Double,
    val modifierReset: Int,
)
