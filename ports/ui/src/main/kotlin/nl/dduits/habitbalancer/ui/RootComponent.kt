package nl.dduits.habitbalancer.ui

import com.arkivanov.decompose.router.stack.ChildStack
import com.arkivanov.decompose.value.Value
import nl.dduits.habitbalancer.ui.list.HabitList

internal interface RootComponent {

    val childStack: Value<ChildStack<*, Child>>

    sealed class Child {
        class HabitListChild(val component: HabitList) : Child()
    }
}
