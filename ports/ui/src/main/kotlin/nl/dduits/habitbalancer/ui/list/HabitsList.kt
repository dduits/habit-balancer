package nl.dduits.habitbalancer.ui.list

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import nl.dduits.habitbalancer.ui.common.HabitBalancerTheme
import kotlin.time.Duration.Companion.days

@Composable
fun HabitsList(
    habits: List<Habit>,
    onHabitClick: (Long) -> Unit
) {
    LazyColumn {
        items(habits) { habit ->
            if (habit.calculatedPointsChange > 0) {
                IncreaseHabitCard(
                    id = habit.id,
                    displayText = habit.name,
                    pointsPerHabit = habit.calculatedPointsChange,
                    durationForPoints = habit.durationForPointsChange,
                    cooldownDuration = null,
                    nextCooldown = null,
                    onClick = onHabitClick
                )
            } else {
                DecreaseHabitCard(
                    id = habit.id,
                    displayText = habit.name,
                    pointsPerHabit = habit.calculatedPointsChange,
                    durationForPoints = habit.durationForPointsChange,
                    cooldownDuration = null,
                    nextCooldown = null,
                    onClick = onHabitClick
                )
            }
            Spacer(modifier = Modifier.height(16.dp))
        }
    }
}

@Preview
@Composable
fun HabitsListPreview() {
    HabitBalancerTheme {
        HabitsList(
            habits = listOf(
                Habit(
                    id = 1,
                    name = "Cold shower",
                    calculatedPointsChange = 100,
                    durationForPointsChange = null,
                    disabled = false
                ),
                Habit(
                    id = 2,
                    name = "Weight lifting",
                    calculatedPointsChange = 200,
                    durationForPointsChange = null,
                    disabled = false
                ),
                Habit(
                    id = 3,
                    name = "Hot shower",
                    calculatedPointsChange = -400,
                    durationForPointsChange = null,
                    disabled = true
                ),
                Habit(
                    id = 4,
                    name = "Watching TV",
                    calculatedPointsChange = -300,
                    durationForPointsChange = 1.days,
                    disabled = false
                )
            ),
            onHabitClick = {}
        )
    }
}
