package nl.dduits.habitbalancer.ui.list

import kotlin.time.Duration

data class Habit(
    val id: Long,
    val name: String,
    val calculatedPointsChange: Int,
    val durationForPointsChange: Duration?,
    val disabled: Boolean
)

data class HabitListState(
    val currentBalance: Int = 0,
    val habits: List<Habit> = emptyList(),
    val createDialogShown: Boolean = false
)
