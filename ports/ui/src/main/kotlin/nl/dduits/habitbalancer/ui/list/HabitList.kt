package nl.dduits.habitbalancer.ui.list

import com.arkivanov.decompose.value.Value

interface HabitList {

    val state: Value<HabitListState>

    fun setup()
    fun onHabitClicked(habitId: Long)
    fun createHabit(
        displayName: String,
        points: Int,
        timeInMinutes: Int
    )
    fun showCreateDialog()
    fun dismissCreateDialog()
}
