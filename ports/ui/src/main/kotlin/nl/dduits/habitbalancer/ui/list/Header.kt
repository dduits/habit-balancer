package nl.dduits.habitbalancer.ui.list

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import nl.dduits.habitbalancer.ui.R
import nl.dduits.habitbalancer.ui.common.HabitBalancerTheme

@Composable
fun Header() {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(id = R.string.habit_list_screen_heading),
            color = MaterialTheme.colors.onBackground,
            fontWeight = FontWeight.SemiBold,
            style = TextStyle(fontSize = 20.sp)
        )
    }
}

@Preview
@Composable
fun HeaderPreview() {
    HabitBalancerTheme {
        Header()
    }
}
