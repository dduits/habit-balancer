package nl.dduits.habitbalancer.ui.list

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import nl.dduits.habitbalancer.ui.R
import nl.dduits.habitbalancer.ui.common.HabitBalancerTheme

@Composable
fun BalanceCard(
    modifier: Modifier = Modifier,
    balance: Int
) {
    Card(
        modifier = modifier.width(209.dp),
        contentColor = MaterialTheme.colors.onPrimary,
        backgroundColor = MaterialTheme.colors.primary
    ) {
        Column(
            modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp)
        ) {
            Text(
                text = stringResource(id = R.string.habit_list_balance_subtitle),
                fontSize = 14.sp
            )
            Text(
                text = stringResource(id = R.string.habit_list_balance_points, balance),
                fontSize = 48.sp,
                fontWeight = FontWeight.Medium
            )
        }
    }
}

@Preview
@Composable
fun BalanceCardPreview() {
    HabitBalancerTheme {
        BalanceCard(
            balance = 200
        )
    }
}
