package nl.dduits.habitbalancer.ui.list

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.arkivanov.decompose.extensions.compose.jetpack.subscribeAsState
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import nl.dduits.habitbalancer.ui.common.HabitBalancerTheme
import kotlin.time.Duration.Companion.minutes

@Composable
fun HabitListScreen(
    modifier: Modifier = Modifier,
    component: HabitList
) {
    LaunchedEffect(key1 = null) {
        component.setup()
    }

    val state by component.state.subscribeAsState()
    HabitListScreen(
        modifier = modifier,
        state = state,
        onHabitClick = { component.onHabitClicked(it) },
        onCreateHabit = { component.createHabit(it.displayName, it.points, it.time) },
        onShowCreateDialog = component::showCreateDialog,
        onDismissCreateDialog = component::dismissCreateDialog
    )
}

@Composable
fun HabitListScreen(
    modifier: Modifier = Modifier,
    state: HabitListState,
    onHabitClick: (Long) -> Unit,
    onCreateHabit: (CreateHabit) -> Unit,
    onShowCreateDialog: () -> Unit,
    onDismissCreateDialog: () -> Unit
) {
    val systemUiController = rememberSystemUiController()

    SideEffect {
        systemUiController.setSystemBarsColor(
            color = Color.Transparent,
            darkIcons = true
        )
    }

    Box(
        modifier = modifier
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(horizontal = 24.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Header()

            BalanceCard(
                modifier = Modifier.padding(vertical = 48.dp),
                state.currentBalance
            )

            if (state.createDialogShown) {
                CreateHabitDialog(
                    submit = onCreateHabit,
                    onDismiss = onDismissCreateDialog
                )
            }

            HabitsList(
                habits = state.habits ?: emptyList(),
                onHabitClick = onHabitClick
            )
        }

        FloatingActionButton(
            modifier = Modifier
                .padding(end = 16.dp, bottom = 16.dp)
                .align(Alignment.BottomEnd),
            onClick = onShowCreateDialog
        ) {
            Icon(
                imageVector = Icons.Rounded.Add,
                contentDescription = null
            )
        }
    }
}

@Preview(widthDp = 360, heightDp = 800)
@Composable
fun HabitListScreenPreview() {
    HabitBalancerTheme {
        Surface(color = MaterialTheme.colors.background) {
            HabitListScreen(
                state = HabitListState(
                    currentBalance = 200,
                    habits =
                    listOf(
                        Habit(
                            id = 0,
                            name = "Meditation",
                            calculatedPointsChange = 100,
                            durationForPointsChange = 30.minutes,
                            disabled = false
                        ),
                        Habit(
                            id = 1,
                            name = "Gaming",
                            calculatedPointsChange = -50,
                            durationForPointsChange = null,
                            disabled = false
                        )
                    )
                ),
                onHabitClick = {},
                onCreateHabit = {},
                onShowCreateDialog = {},
                onDismissCreateDialog = {}
            )
        }
    }
}
