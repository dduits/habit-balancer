package nl.dduits.habitbalancer.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.arkivanov.decompose.ComponentContext
import com.arkivanov.decompose.ExperimentalDecomposeApi
import com.arkivanov.decompose.extensions.compose.jetpack.stack.Children
import com.arkivanov.decompose.extensions.compose.jetpack.stack.animation.slide
import com.arkivanov.decompose.extensions.compose.jetpack.stack.animation.stackAnimation
import nl.dduits.habitbalancer.ui.common.HabitBalancerTheme
import nl.dduits.habitbalancer.ui.list.HabitListScreen

@Composable
fun HabitBalancerRoot(
    modifier: Modifier = Modifier,
    context: ComponentContext
) {
    HabitBalancerTheme {
        val rootComponent = DefaultRootComponent(context)

        Surface(
            modifier = modifier,
            color = MaterialTheme.colors.background
        ) {
            Column {
                HabitBalancerChildren(
                    modifier = Modifier
                        .systemBarsPadding()
                        .fillMaxSize(),
                    rootComponent = rootComponent
                )
            }
        }
    }
}

@Composable
@OptIn(ExperimentalDecomposeApi::class)
internal fun HabitBalancerChildren(
    modifier: Modifier = Modifier,
    rootComponent: DefaultRootComponent
) {
    Children(
        stack = rootComponent.childStack,
        animation = stackAnimation(slide())
    ) {
        when (val instance = it.instance) {
            is RootComponent.Child.HabitListChild -> HabitListScreen(
                modifier = modifier,
                component = instance.component
            )
        }
    }
}
