package nl.dduits.habitbalancer.ui.list

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import nl.dduits.habitbalancer.ui.R
import nl.dduits.habitbalancer.ui.common.HabitBalancerTheme
import kotlin.time.Duration
import kotlin.time.Duration.Companion.minutes

@Composable
fun IncreaseHabitCard(
    modifier: Modifier = Modifier,
    id: Long,
    displayText: String,
    pointsPerHabit: Int,
    durationForPoints: Duration?,
    cooldownDuration: Duration?,
    nextCooldown: Pair<Int, Duration>?,
    onClick: (Long) -> Unit
) {
    HabitCard(
        modifier = modifier,
        id = id,
        displayText = displayText,
        pointsPerHabit = pointsPerHabit,
        durationForPoints = durationForPoints,
        cooldownDuration = cooldownDuration,
        nextCooldown = nextCooldown,
        contentColor = MaterialTheme.colors.onPrimary,
        backgroundColor = MaterialTheme.colors.primary,
        onClick = onClick
    )
}

@Composable
fun DecreaseHabitCard(
    modifier: Modifier = Modifier,
    id: Long,
    displayText: String,
    pointsPerHabit: Int,
    durationForPoints: Duration?,
    cooldownDuration: Duration?,
    nextCooldown: Pair<Int, Duration>?,
    onClick: (Long) -> Unit
) {
    HabitCard(
        modifier = modifier,
        id = id,
        displayText = displayText,
        pointsPerHabit = pointsPerHabit,
        durationForPoints = durationForPoints,
        cooldownDuration = cooldownDuration,
        nextCooldown = nextCooldown,
        contentColor = MaterialTheme.colors.onSecondary,
        backgroundColor = MaterialTheme.colors.secondary,
        onClick = onClick
    )
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HabitCard(
    modifier: Modifier = Modifier,
    id: Long,
    displayText: String,
    pointsPerHabit: Int,
    durationForPoints: Duration?,
    cooldownDuration: Duration?,
    nextCooldown: Pair<Int, Duration>?,
    contentColor: Color,
    backgroundColor: Color,
    onClick: (Long) -> Unit
) {
    Card(
        modifier = modifier,
        backgroundColor = backgroundColor,
        contentColor = contentColor,
        onClick = { onClick(id) }
    ) {
        Row(
            modifier = Modifier.padding(16.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(Modifier.weight(1f)) {
                Text(
                    text = displayText,
                    fontWeight = FontWeight.Medium,
                    style = TextStyle(fontSize = 18.sp)
                )
                if (nextCooldown != null) {
                    Text(
                        text = stringResource(id = R.string.habit_list_item_next_cooldown, nextCooldown.first, nextCooldown.second),
                        style = TextStyle(fontSize = 12.sp)
                    )
                }
            }
            Column(
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = stringResource(id = R.string.habit_list_item_points, pointsPerHabit),
                    fontWeight = FontWeight.Medium,
                    style = TextStyle(fontSize = 16.sp)
                )
                if (durationForPoints != null) {
                    Text(
                        text = "$durationForPoints",
                        style = TextStyle(fontSize = 12.sp)
                    )
                }
            }
        }
    }
}

@Preview(widthDp = 400)
@Composable
fun IncreaseHabitCardPreview() {
    HabitBalancerTheme {
        IncreaseHabitCard(
            id = 0,
            displayText = "Exercise",
            pointsPerHabit = 100,
            durationForPoints = 15.minutes,
            cooldownDuration = null,
            nextCooldown = null,
            onClick = {}
        )
    }
}

@Preview(widthDp = 400)
@Composable
fun DecreaseHabitCardPreview() {
    HabitBalancerTheme {
        DecreaseHabitCard(
            id = 0,
            displayText = "Gaming",
            pointsPerHabit = -100,
            durationForPoints = null,
            cooldownDuration = null,
            nextCooldown = null
        ) {}
    }
}
