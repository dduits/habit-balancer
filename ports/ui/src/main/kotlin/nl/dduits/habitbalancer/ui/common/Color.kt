package nl.dduits.habitbalancer.ui.common

import androidx.compose.ui.graphics.Color

val PunchyBlue = Color(0xFF5792FF)

val LightBlue = Color(0xFFAFCBFF)

val LighterBlue = Color(0xFFCEDFFF)

val DarkBlue = Color(0xFF5C75A5)

val DeepBlue = Color(0xFF3A465C)
