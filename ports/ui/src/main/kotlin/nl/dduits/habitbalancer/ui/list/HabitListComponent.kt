package nl.dduits.habitbalancer.ui.list

import android.util.Log
import com.arkivanov.decompose.ComponentContext
import com.arkivanov.decompose.value.MutableValue
import com.arkivanov.decompose.value.Value
import com.arkivanov.decompose.value.reduce
import com.arkivanov.essenty.lifecycle.LifecycleOwner
import com.arkivanov.essenty.lifecycle.doOnDestroy
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import nl.dduits.habitbalancer.core.AppDispatchers
import nl.dduits.habitbalancer.core.CalculatePoints
import nl.dduits.habitbalancer.core.ChangeHabitBalance
import nl.dduits.habitbalancer.core.CreateHabit
import nl.dduits.habitbalancer.core.GetHabits
import nl.dduits.habitbalancer.core.Habit.PointsPerHabit
import nl.dduits.habitbalancer.core.ObserveHabitPointsBalance
import nl.dduits.habitbalancer.core.PointsCalculation
import org.koin.java.KoinJavaComponent.get
import kotlin.coroutines.CoroutineContext
import kotlin.time.Duration.Companion.minutes

fun LifecycleOwner.coroutineScope(context: CoroutineContext): CoroutineScope {
    val scope = CoroutineScope(context + SupervisorJob())
    lifecycle.doOnDestroy(scope::cancel)
    return scope
}

class HabitListComponent(
    val componentContext: ComponentContext,
    dispatchers: AppDispatchers = get(AppDispatchers::class.java),
    private val changeHabitBalance: ChangeHabitBalance = get(ChangeHabitBalance::class.java),
    private val observeHabitPointsBalance: ObserveHabitPointsBalance = get(ObserveHabitPointsBalance::class.java),
    private val calculatePoints: CalculatePoints = get(CalculatePoints::class.java),
    private val createHabit: CreateHabit = get(CreateHabit::class.java),
    private val getHabits: GetHabits = get(GetHabits::class.java),
) : HabitList, ComponentContext by componentContext {

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("HabitListComponent", "Error occurred.", throwable)
    }

    private val coroutineScope = coroutineScope(dispatchers.default + exceptionHandler)

    private val time = flow {
        while (true) {
            emit(Clock.System.now())
            delay(1000)
        }
    }

    private val _state = MutableValue(HabitListState())
    override val state: Value<HabitListState> = _state

    private fun collectBalance() {
        coroutineScope.launch {
            observeHabitPointsBalance.getBalance()
                .collectLatest { balance ->
                    _state.reduce { it.copy(currentBalance = balance) }
                }
        }
    }

    private fun collectHabits() {
        coroutineScope.launch {
            combine(time, getHabits.get()) { time, habits -> combineToHabits(habits, time) }
                .collectLatest { habits ->
                    _state.reduce { it.copy(habits = habits) }
                }
        }
    }

    private suspend fun combineToHabits(
        habits: List<nl.dduits.habitbalancer.core.Habit>,
        time: Instant
    ) = habits
        .map { habit ->
            val calculation = calculatePoints.calculate(habit, time)
            combineToHabit(habit, calculation)
        }
        .sortedByDescending { it.calculatedPointsChange }

    private fun combineToHabit(
        habit: nl.dduits.habitbalancer.core.Habit,
        calculation: PointsCalculation
    ): Habit = Habit(
        id = habit.id,
        name = habit.displayText,
        calculatedPointsChange = calculation.pointsChange,
        durationForPointsChange = calculation.durationForPointsChange,
        disabled = calculation.disabled
    )

    override fun setup() {
        collectBalance()
        collectHabits()
    }

    override fun onHabitClicked(habitId: Long) {

        val habit = _state.value.habits.find { habitId == it.id }
        if (habit == null || habit.disabled) {
            return
        }

        coroutineScope.launch {
            val now = Clock.System.now()
            changeHabitBalance.change(habitId, now)
        }
    }

    override fun createHabit(
        displayName: String,
        points: Int,
        timeInMinutes: Int,
    ) {
        coroutineScope.launch {
            createHabit.create(
                nl.dduits.habitbalancer.core.Habit(
                    id = 0,
                    displayText = displayName,
                    pointsPerHabit = points,
                    pointsModifier = null,
                    pointsPerHabitType = when {
                        timeInMinutes > 0 -> PointsPerHabit.Overtime(timeInMinutes.minutes)
                        else -> PointsPerHabit.Event
                    }
                )
            )
        }
        _state.reduce { it.copy(createDialogShown = false) }
    }

    override fun showCreateDialog() {
        _state.reduce { it.copy(createDialogShown = true) }
    }

    override fun dismissCreateDialog() {
        _state.reduce { it.copy(createDialogShown = false) }
    }
}
