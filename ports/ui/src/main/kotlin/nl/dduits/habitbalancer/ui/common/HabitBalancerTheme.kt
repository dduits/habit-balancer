package nl.dduits.habitbalancer.ui.common

import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

private val LightColorPalette = lightColors(
    primary = LightBlue,
    secondary = DarkBlue,
    background = LighterBlue,
    surface = Color.White,
    onPrimary = DeepBlue,
    onSecondary = Color.White,
    onBackground = DeepBlue,
    onSurface = DeepBlue
)

@Composable
fun HabitBalancerTheme(content: @Composable () -> Unit) {
    MaterialTheme(
        colors = LightColorPalette,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}
