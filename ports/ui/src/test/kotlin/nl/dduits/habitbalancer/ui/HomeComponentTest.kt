package nl.dduits.habitbalancer.ui

import com.arkivanov.decompose.DefaultComponentContext
import com.arkivanov.essenty.instancekeeper.InstanceKeeperDispatcher
import com.arkivanov.essenty.lifecycle.LifecycleRegistry
import com.arkivanov.essenty.statekeeper.StateKeeperDispatcher
import io.kotest.core.spec.style.FunSpec
import io.kotest.core.test.testCoroutineScheduler
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import nl.dduits.habitbalancer.core.AppDispatchers
import nl.dduits.habitbalancer.core.CalculatePoints
import nl.dduits.habitbalancer.core.ChangeHabitBalance
import nl.dduits.habitbalancer.core.CreateHabit
import nl.dduits.habitbalancer.core.GetHabits
import nl.dduits.habitbalancer.core.Habit
import nl.dduits.habitbalancer.core.HabitCooldownRepository
import nl.dduits.habitbalancer.core.HabitRepository
import nl.dduits.habitbalancer.core.ObserveHabitPointsBalance
import nl.dduits.habitbalancer.core.PointsBalanceRepository
import nl.dduits.habitbalancer.core.calculatePoints
import nl.dduits.habitbalancer.core.changeHabitBalance
import nl.dduits.habitbalancer.core.createHabit
import nl.dduits.habitbalancer.core.getHabits
import nl.dduits.habitbalancer.core.observeHabitPointsBalance
import nl.dduits.habitbalancer.memory.InMemoryHabitCooldownRepository
import nl.dduits.habitbalancer.memory.InMemoryHabitRepository
import nl.dduits.habitbalancer.memory.InMemoryPointsBalanceRepository
import nl.dduits.habitbalancer.ui.list.HabitList
import nl.dduits.habitbalancer.ui.list.HabitListComponent
import kotlin.time.Duration.Companion.minutes

@OptIn(ExperimentalStdlibApi::class, ExperimentalCoroutinesApi::class)
internal class HomeComponentTest : FunSpec({

    coroutineTestScope = true

    test("showCreateDialog should set createDialogShown to true") {
        val component = createComponent()
        component.state.value.createDialogShown shouldBe false
        component.showCreateDialog()
        component.state.value.createDialogShown shouldBe true
    }

    test("dismissCreateDialog should set createDialogShown to false") {
        val component = createComponent()
        component.showCreateDialog()
        component.state.value.createDialogShown shouldBe true
        component.dismissCreateDialog()
        component.state.value.createDialogShown shouldBe false
    }

    test("createHabit should dismiss create dialog") {
        val component = createComponent()
        component.showCreateDialog()
        component.state.value.createDialogShown shouldBe true
        component.createHabit("Hot Shower", -100, 0)
        component.state.value.createDialogShown shouldBe false
    }

    test("createHabit should create habit") {

        val habitRepository = InMemoryHabitRepository()
        val getHabits = getHabits(habitRepository)
        val component = createComponent(
            habitRepository = habitRepository,
            getHabits = getHabits,
        )

        component.createHabit(
            displayName = "Gaming",
            points = -100,
            timeInMinutes = 0
        )

        testCoroutineScheduler.advanceUntilIdle()

        getHabits.get().first().first().let { habit ->
            habit.displayText shouldBe "Gaming"
            habit.pointsPerHabit shouldBe -100
        }
    }

    test("createHabit with 15 minutes time should create habit") {

        val habitRepository = InMemoryHabitRepository()
        val getHabits = getHabits(habitRepository)

        val dispatcher = coroutineContext.get(CoroutineDispatcher.Key)!!
        println(dispatcher)

        val dispatchers = AppDispatchers(
            default = coroutineContext.get(CoroutineDispatcher.Key)!!,
            io = coroutineContext.get(CoroutineDispatcher.Key)!!
        )

        val component = createComponent(
            dispatchers = dispatchers,
            habitRepository = habitRepository,
            getHabits = getHabits,
        )

        component.createHabit(
            displayName = "Gaming",
            points = -100,
            timeInMinutes = 30
        )

        testCoroutineScheduler.advanceUntilIdle()

        getHabits.get().first().first().let { habit ->
            habit.displayText shouldBe "Gaming"
            habit.pointsPerHabit shouldBe -100
            habit.pointsPerHabitType shouldBe Habit.PointsPerHabit.Overtime(30.minutes)
        }
    }
})

@OptIn(ExperimentalStdlibApi::class)
private fun CoroutineScope.createComponent(
    stateKeeper: StateKeeperDispatcher = StateKeeperDispatcher(),
    instanceKeeper: InstanceKeeperDispatcher = InstanceKeeperDispatcher(),
    dispatchers: AppDispatchers = AppDispatchers(
        default = coroutineContext.get(CoroutineDispatcher.Key)!!,
        io = coroutineContext.get(CoroutineDispatcher.Key)!!
    ),
    habitRepository: HabitRepository = InMemoryHabitRepository(),
    pointsBalanceRepository: PointsBalanceRepository = InMemoryPointsBalanceRepository(),
    habitCooldownRepository: HabitCooldownRepository = InMemoryHabitCooldownRepository(),
    changeHabitBalance: ChangeHabitBalance = changeHabitBalance(
        persistenceRepository = pointsBalanceRepository,
        habitRepository = habitRepository,
        habitCooldownRepository = habitCooldownRepository
    ),
    observeHabitPointsBalance: ObserveHabitPointsBalance = observeHabitPointsBalance(
        repository = pointsBalanceRepository
    ),
    calculatePoints: CalculatePoints = calculatePoints(
        balanceRepository = pointsBalanceRepository,
        cooldownRepository = habitCooldownRepository
    ),
    createHabit: CreateHabit = createHabit(habitRepository),
    getHabits: GetHabits = getHabits(habitRepository),
): HabitList {
    return HabitListComponent(
        componentContext = DefaultComponentContext(
            lifecycle = LifecycleRegistry(),
            stateKeeper = stateKeeper,
            instanceKeeper = instanceKeeper
        ),
        dispatchers = dispatchers,
        changeHabitBalance = changeHabitBalance,
        observeHabitPointsBalance = observeHabitPointsBalance,
        calculatePoints = calculatePoints,
        createHabit = createHabit,
        getHabits = getHabits,
    )
}
