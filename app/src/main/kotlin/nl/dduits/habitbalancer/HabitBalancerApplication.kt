package nl.dduits.habitbalancer

import android.app.Application
import nl.dduits.habitbalancer.room.coroutineModule
import nl.dduits.habitbalancer.room.interactorModule
import nl.dduits.habitbalancer.room.roomModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

internal class HabitBalancerApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startDependencyInjection()
    }

    private fun startDependencyInjection() {
        startKoin {
            androidContext(this@HabitBalancerApplication)
            modules(coroutineModule, roomModule, interactorModule)
        }
    }
}
