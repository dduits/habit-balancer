package nl.dduits.habitbalancer.room

import kotlinx.coroutines.Dispatchers
import nl.dduits.habitbalancer.core.AppDispatchers
import nl.dduits.habitbalancer.core.HabitCooldownRepository
import nl.dduits.habitbalancer.core.HabitRepository
import nl.dduits.habitbalancer.core.PointsBalanceRepository
import nl.dduits.habitbalancer.core.changeHabitBalance
import nl.dduits.habitbalancer.core.createHabit
import nl.dduits.habitbalancer.core.calculatePoints
import nl.dduits.habitbalancer.core.getHabits
import nl.dduits.habitbalancer.core.observeHabitPointsBalance
import nl.dduits.habitbalancer.core.undoLastHabit
import org.koin.dsl.module

internal val coroutineModule = module {
    single { AppDispatchers() }
}

@Suppress("USELESS_CAST")
internal val roomModule = module {
    // Database
    single { HabitBalancerRoomDatabaseBuilderImpl(get()) as HabitBalancerRoomDatabaseBuilder }
    single { get<HabitBalancerRoomDatabaseBuilder>().create() as HabitBalancerRoomDatabase }

    // DAOs
    single { get<HabitBalancerRoomDatabase>().getHabitBalanceDao() as HabitBalanceDao }
    single { get<HabitBalancerRoomDatabase>().getHabitDao() as HabitDao }
    single { get<HabitBalancerRoomDatabase>().getHabitCooldownDao() as HabitCooldownDao }

    // Repositories
    single { RoomPointsBalanceRepository(get()) as PointsBalanceRepository }
    single { RoomHabitRepository(get()) as HabitRepository }
    single { RoomHabitCooldownRepository(get()) as HabitCooldownRepository }
}

internal val interactorModule = module {
    single { calculatePoints(get(), get()) }
    single { observeHabitPointsBalance(get()) }
    single { changeHabitBalance(get(), get(), get()) }
    single { undoLastHabit(get()) }
    single { createHabit(get()) }
    single { getHabits(get()) }
}
