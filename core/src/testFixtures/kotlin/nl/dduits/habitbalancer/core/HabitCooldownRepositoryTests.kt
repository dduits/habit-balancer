package nl.dduits.habitbalancer.core.repo

import io.kotest.core.spec.style.funSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.Month
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant
import nl.dduits.habitbalancer.core.TestHabitCooldowns.coldShowerCooldown
import nl.dduits.habitbalancer.core.TestHabitCooldowns.exerciseCooldown
import nl.dduits.habitbalancer.core.TestHabitCooldowns.meditationCooldown
import nl.dduits.habitbalancer.core.TestHabitCooldowns.yogaCooldown
import nl.dduits.habitbalancer.core.TestHabits.coldShower
import nl.dduits.habitbalancer.core.TestHabits.exercise
import nl.dduits.habitbalancer.core.TestHabits.meditation
import nl.dduits.habitbalancer.core.TestHabits.yoga
import nl.dduits.habitbalancer.core.HabitCooldownRepository
import nl.dduits.habitbalancer.core.HabitRepository

fun habitCooldownRepositoryTests(
    habitRepository: HabitRepository,
    cooldownRepository: HabitCooldownRepository
) = funSpec {

    test("cooldown by ID should be the same as registered cooldown") {
        habitRepository.createHabit(meditation)
        cooldownRepository.registerHabitCooldown(meditationCooldown)
        cooldownRepository.getHabitCooldownById(meditationCooldown.habitId) shouldBe meditationCooldown
    }

    test("cooldown by ID should return null when no cooldown exists") {
        habitRepository.createHabit(meditation)
        cooldownRepository.getHabitCooldownById(meditationCooldown.habitId) shouldBe null
    }

    test("should be able to query for latest cooldown") {
        habitRepository.createHabit(meditation)

        val habitCooldownCopy = meditationCooldown.copy(
            violations = meditationCooldown.violations + 1,
            violationInstant = LocalDateTime(2022, Month.SEPTEMBER, 20, 8, 0)
                .toInstant(TimeZone.UTC)
        )

        cooldownRepository.registerHabitCooldown(meditationCooldown)
        cooldownRepository.registerHabitCooldown(habitCooldownCopy)
        cooldownRepository.getHabitCooldownById(meditationCooldown.habitId) shouldBe habitCooldownCopy
    }

    test("should be able to query for cooldowns with list of IDs") {
        listOf(meditation, yoga, coldShower, exercise)
            .forEach { habitRepository.createHabit(it) }

        listOf(
            meditationCooldown,
            yogaCooldown,
            coldShowerCooldown,
            exerciseCooldown
        ).forEach { cooldownRepository.registerHabitCooldown(it) }

        val habits = listOf(meditationCooldown, coldShowerCooldown, exerciseCooldown)
        cooldownRepository.getHabitCooldownsWithIds(habits.map { it.habitId })
            .shouldContainExactly(habits)
    }
}
