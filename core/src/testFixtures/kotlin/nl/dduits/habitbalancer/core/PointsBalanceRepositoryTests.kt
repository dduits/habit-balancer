package nl.dduits.habitbalancer.core.repo

import io.kotest.assertions.throwables.shouldThrowExactly
import io.kotest.core.spec.style.funSpec
import io.kotest.matchers.shouldBe
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.Month
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant
import nl.dduits.habitbalancer.core.dummyInstant
import nl.dduits.habitbalancer.core.BalanceChange
import nl.dduits.habitbalancer.core.PointsBalanceRepository
import kotlin.time.Duration.Companion.hours

fun pointsBalanceRepositoryTests(repository: PointsBalanceRepository) = funSpec {

    test("habit balance should be zero by default") {
        repository.getBalance() shouldBe 0
    }

    test("habit balance should be 200 after it was increased with 200 points") {
        repository.changeBalance(200, dummyInstant)
        repository.getBalance() shouldBe 200
    }

    test("habit balance should be increased and decreased") {

        repository.changeBalance(150, dummyInstant)
        repository.getBalance() shouldBe 150

        repository.changeBalance(-100, dummyInstant)
        repository.getBalance() shouldBe 50
    }

    test("should error when balance change is zero") {
        shouldThrowExactly<IllegalArgumentException> {
            repository.changeBalance(0, dummyInstant)
        }
    }

    test("balance should be 50 after remove newest") {
        with(repository) {
            val initialInstant = LocalDateTime(2022, Month.FEBRUARY, 1, 12, 0)
                .toInstant(TimeZone.UTC)

            changeBalance(50, initialInstant)
            changeBalance(-50, initialInstant + 3.hours)
            removeNewestBalanceChange()

            getBalance() shouldBe 50
        }
    }

    test("should error when attempting to remove nonexistent latest balance change") {
        shouldThrowExactly<IllegalStateException> { repository.removeNewestBalanceChange() }
        repository.getBalance() shouldBe 0
    }

    test("should return newest balance change") {
        val initialInstant = LocalDateTime(2022, Month.FEBRUARY, 1, 12, 0)
            .toInstant(TimeZone.UTC)

        repository.changeBalance(50, initialInstant)
        repository.changeBalance(100, initialInstant + 1.hours)
        repository.changeBalance(-120, initialInstant + 3.hours)

        val latestInstant = initialInstant + 8.hours
        repository.changeBalance(-10, latestInstant)

        repository.getNewestBalanceChange() shouldBe BalanceChange(-10, latestInstant)
    }
}
