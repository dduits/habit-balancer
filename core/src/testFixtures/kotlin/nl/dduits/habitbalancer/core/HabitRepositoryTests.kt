package nl.dduits.habitbalancer.core.repo

import app.cash.turbine.test
import io.kotest.core.spec.style.funSpec
import io.kotest.matchers.collections.shouldContainAll
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.flow.first
import nl.dduits.habitbalancer.core.TestHabits.gaming
import nl.dduits.habitbalancer.core.TestHabits.meditation
import nl.dduits.habitbalancer.core.TestHabits.walk
import nl.dduits.habitbalancer.core.HabitRepository

fun habitRepositoryTests(repository: HabitRepository) = funSpec {

    test("habit can be created and is returned in a list") {
        repository.createHabit(meditation)
        repository.getHabits().first() shouldContainExactly listOf(meditation)
    }

    test("habit can be returned by ID") {
        repository.createHabit(meditation)
        repository.getHabitById(meditation.id) shouldBe meditation
    }

    test("habits should be collected in sequence").config(coroutineTestScope = true) {
        repository.getHabits().test {
            awaitItem() shouldBe emptyList()

            repository.createHabit(meditation)
            awaitItem() shouldBe listOf(meditation)

            repository.createHabit(walk)
            awaitItem() shouldContainExactlyInAnyOrder listOf(meditation, walk)

            repository.createHabit(gaming)
            awaitItem() shouldContainExactlyInAnyOrder listOf(meditation, walk, gaming)

            cancelAndIgnoreRemainingEvents()
        }
    }
}
