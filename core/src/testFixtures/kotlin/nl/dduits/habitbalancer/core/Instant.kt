package nl.dduits.habitbalancer.core

import kotlinx.datetime.Instant

val dummyInstant = Instant.fromEpochSeconds(0)
