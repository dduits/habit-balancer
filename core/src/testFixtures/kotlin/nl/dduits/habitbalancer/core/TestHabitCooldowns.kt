package nl.dduits.habitbalancer.core

import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.Month
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant

object TestHabitCooldowns {

    val meditationCooldown
        get() = HabitCooldown(
            habitId = TestHabits.meditation.id,
            violations = 3,
            violationInstant = LocalDateTime(2022, Month.SEPTEMBER, 13, 8, 0)
                .toInstant(TimeZone.UTC)
        )

    val yogaCooldown
        get() = HabitCooldown(
            habitId = TestHabits.yoga.id,
            violations = 1,
            violationInstant = LocalDateTime(2022, Month.SEPTEMBER, 12, 12, 0)
                .toInstant(TimeZone.UTC)
        )

    val coldShowerCooldown
        get() = HabitCooldown(
            habitId = TestHabits.coldShower.id,
            violations = 4,
            violationInstant = LocalDateTime(2022, Month.SEPTEMBER, 9, 20, 0)
                .toInstant(TimeZone.UTC)
        )

    val exerciseCooldown
        get() = HabitCooldown(
            habitId = TestHabits.exercise.id,
            violations = 2,
            violationInstant = LocalDateTime(2022, Month.SEPTEMBER, 10, 21, 15)
                .toInstant(TimeZone.UTC)
        )
}
