package nl.dduits.habitbalancer.core

import nl.dduits.habitbalancer.core.Habit.PointsModifier
import nl.dduits.habitbalancer.core.Habit.PointsPerHabit
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes

object TestHabits {

    val meditation
        get() = Habit(
            id = 1,
            displayText = "Meditation",
            pointsPerHabit = 100,
            pointsPerHabitType = PointsPerHabit.Overtime(15.minutes),
            pointsModifier = PointsModifier(
                multiplier = .5,
                resetPeriod = 2.days
            )
        )

    val snacking
        get() = Habit(
            id = 2,
            displayText = "Snacking",
            pointsPerHabit = -10,
            pointsPerHabitType = PointsPerHabit.Event,
            pointsModifier = PointsModifier(
                multiplier = 1.5,
                resetPeriod = 1.hours
            )
        )

    val gaming
        get() = Habit(
            id = 3,
            displayText = "Gaming",
            pointsPerHabit = -100,
            pointsPerHabitType = PointsPerHabit.Event,
            pointsModifier = PointsModifier(
                multiplier = 2.0,
                resetPeriod = 2.days
            )
        )

    val smoking
        get() = Habit(
            id = 4,
            displayText = "Smoking",
            pointsPerHabit = -200,
            pointsPerHabitType = PointsPerHabit.Event,
            pointsModifier = PointsModifier(
                multiplier = 2.0,
                resetPeriod = 2.days
            )
        )

    val yoga
        get() = Habit(
            id = 5,
            displayText = "Yoga",
            pointsPerHabit = 200,
            pointsPerHabitType = PointsPerHabit.Overtime(2.hours),
            pointsModifier = null
        )

    val walk
        get() = Habit(
            id = 7,
            displayText = "Walking",
            pointsPerHabit = 100,
            pointsPerHabitType = PointsPerHabit.Overtime(15.minutes),
            pointsModifier = PointsModifier(
                multiplier = 1.5,
                resetPeriod = 1.days
            )
        )

    val journal: Habit
        get() = Habit(
            id = 8,
            displayText = "Journaling",
            pointsPerHabit = 5,
            pointsPerHabitType = PointsPerHabit.Event,
            pointsModifier = null
        )

    val pomodoro
        get() = Habit(
            id = 9,
            displayText = "Pomodoro for 25 minutes",
            pointsPerHabit = 100,
            pointsPerHabitType = PointsPerHabit.Overtime(25.minutes),
            pointsModifier = null
        )

    val coldShower
        get() = Habit(
            id = 10,
            displayText = "Cold Shower",
            pointsPerHabit = 50,
            pointsPerHabitType = PointsPerHabit.Overtime(15.minutes),
            pointsModifier = null
        )

    val exercise
        get() = Habit(
            id = 11,
            displayText = "Exercise",
            pointsPerHabit = 100,
            pointsPerHabitType = PointsPerHabit.Overtime(15.minutes),
            pointsModifier = PointsModifier(
                multiplier = 1.5,
                resetPeriod = 1.days
            )
        )
}
