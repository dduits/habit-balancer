package nl.dduits.habitbalancer.core

import app.cash.turbine.test
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import nl.dduits.habitbalancer.core.TestHabits.gaming
import nl.dduits.habitbalancer.core.TestHabits.meditation
import nl.dduits.habitbalancer.memory.InMemoryHabitRepository

internal class GetHabitsTest : FunSpec({

    val habitRepository = InMemoryHabitRepository()

    val createHabit = createHabit(habitRepository)
    val getHabits: GetHabits = getHabits(habitRepository)

    test("get habits should return a list of habits") {

        listOf(meditation, gaming)
            .forEach { createHabit.create(it) }

        getHabits.get().test {
            awaitItem() shouldBe listOf(meditation, gaming)
            cancelAndIgnoreRemainingEvents()
        }
    }

})