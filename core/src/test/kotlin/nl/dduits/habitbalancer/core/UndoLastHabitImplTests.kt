package nl.dduits.habitbalancer.core

import io.kotest.assertions.throwables.shouldThrowExactly
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.flow.first
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.Month
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant
import nl.dduits.habitbalancer.memory.InMemoryHabitCooldownRepository
import nl.dduits.habitbalancer.memory.InMemoryHabitRepository
import nl.dduits.habitbalancer.memory.InMemoryPointsBalanceRepository
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

internal class UndoLastHabitImplTests : FunSpec({

    val balanceRepository = InMemoryPointsBalanceRepository()
    val habitRepository = InMemoryHabitRepository()
    val habitCooldownRepository = InMemoryHabitCooldownRepository()

    val createHabit = createHabit(habitRepository)
    val changeBalance =
        changeHabitBalance(balanceRepository, habitRepository, habitCooldownRepository)
    val observeBalance = observeHabitPointsBalance(balanceRepository)
    val undoLastHabit = undoLastHabit(balanceRepository)

    test("undo last habit should revert points balance to the previous value") {
        createHabit.create(TestHabits.walk)

        val initialInstant = LocalDateTime(2022, Month.JANUARY, 20, 8, 0)
            .toInstant(TimeZone.UTC)

        changeBalance.change(TestHabits.walk.id, initialInstant)
        observeBalance.getBalance().first() shouldBe 100

        changeBalance.change(TestHabits.walk.id, initialInstant + 10.minutes)
        observeBalance.getBalance().first() shouldBe 250

        val momentOfUndo = initialInstant + 10.minutes + 6.seconds
        undoLastHabit.undo(momentOfUndo)

        observeBalance.getBalance().first() shouldBe 100
    }

    test(
        "point balance should remain unchanged if undo was attempted outside of " +
            "the 10 second time frame"
    ) {
        createHabit.create(TestHabits.walk)

        val initialInstant = LocalDateTime(2022, Month.JANUARY, 12, 20, 0)
            .toInstant(TimeZone.UTC)

        changeBalance.change(TestHabits.walk.id, initialInstant)
        observeBalance.getBalance().first() shouldBe 100

        changeBalance.change(TestHabits.walk.id, initialInstant + 10.minutes)
        observeBalance.getBalance().first() shouldBe 250

        val momentOfUndo = initialInstant + 15.minutes
        shouldThrowExactly<IllegalStateException> {
            undoLastHabit.undo(momentOfUndo)
        }

        observeBalance.getBalance().first() shouldBe 250
    }

    test("undo habit should error when no balance changes exist") {
        shouldThrowExactly<IllegalStateException> {

            val instant = LocalDateTime(2022, Month.JULY, 4, 10, 0)
                .toInstant(TimeZone.UTC)

            undoLastHabit.undo(instant)
        }
    }
})
