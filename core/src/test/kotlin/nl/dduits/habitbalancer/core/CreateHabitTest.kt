package nl.dduits.habitbalancer.core

import io.kotest.assertions.throwables.shouldThrowExactly
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.flow.first
import nl.dduits.habitbalancer.core.Habit.PointsModifier
import nl.dduits.habitbalancer.core.Habit.PointsPerHabit
import nl.dduits.habitbalancer.memory.InMemoryHabitRepository
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

internal class CreateHabitTest : FunSpec({

    val habitRepository = InMemoryHabitRepository()
    val createHabit = createHabit(habitRepository)

    test("habit with cooldown should be the created") {

        val habit = Habit(
            id = 1,
            displayText = "Meditation",
            pointsPerHabit = 100,
            pointsPerHabitType = PointsPerHabit.Overtime(15.minutes),
            pointsModifier = PointsModifier(
                multiplier = .5,
                resetPeriod = 2.days
            )
        )

        createHabit.create(habit)
        habitRepository.getHabits().first() shouldBe listOf(habit)
    }

    test("habit without cooldown should be the created") {

        val habit = Habit(
            id = 8,
            displayText = "Journaling",
            pointsPerHabit = 5,
            pointsPerHabitType = PointsPerHabit.Event,
            pointsModifier = null
        )

        createHabit.create(habit)
        habitRepository.getHabits().first() shouldBe listOf(habit)
    }

    listOf(
        "",
        "       "
    ).forEach { displayText ->
        test("habit creation should error when display text is \"$displayText\"") {

            val habit = TestHabits.meditation.copy(displayText = displayText)

            shouldThrowExactly<IllegalArgumentException> {
                createHabit.create(habit)
            }
        }
    }

    test("habit creation should error when it rewards zero points") {

        val habit = TestHabits.meditation.copy(pointsPerHabit = 0)

        shouldThrowExactly<IllegalArgumentException> {
            createHabit.create(habit)
        }
    }

    listOf(
        (-15).minutes,
        0.seconds
    ).forEach { duration ->
        test("habit creation should error when points per habit duration is $duration") {

            val habit = TestHabits.pomodoro
                .copy(pointsPerHabitType = PointsPerHabit.Overtime(duration))

            shouldThrowExactly<IllegalArgumentException> {
                createHabit.create(habit)
            }
        }
    }

    listOf(
        (-20).minutes,
        0.minutes
    ).forEach { duration ->
        test("habit creation should error when cooldown duration is $duration") {

            val habit = TestHabits.pomodoro
                .copy(
                    pointsModifier = PointsModifier(
                        multiplier = 1.1,
                        resetPeriod = duration
                    )
                )

            shouldThrowExactly<IllegalArgumentException> {
                createHabit.create(habit)
            }
        }
    }

    test("habit creation should error when cooldown multiplier is 0") {

        val habit = TestHabits.pomodoro
            .copy(
                pointsModifier = PointsModifier(
                    multiplier = 0.0,
                    resetPeriod = 10.minutes
                )
            )

        shouldThrowExactly<IllegalArgumentException> {
            createHabit.create(habit)
        }
    }
})
