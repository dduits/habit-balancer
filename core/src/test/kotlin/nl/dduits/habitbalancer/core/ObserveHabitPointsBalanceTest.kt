package nl.dduits.habitbalancer.core

import app.cash.turbine.test
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.flow.first
import nl.dduits.habitbalancer.core.TestHabits.snacking
import nl.dduits.habitbalancer.core.TestHabits.walk
import nl.dduits.habitbalancer.memory.InMemoryHabitCooldownRepository
import nl.dduits.habitbalancer.memory.InMemoryHabitRepository
import nl.dduits.habitbalancer.memory.InMemoryPointsBalanceRepository

internal class ObserveHabitPointsBalanceTest : FunSpec({

    val pointsBalanceRepository = InMemoryPointsBalanceRepository()
    val habitRepository = InMemoryHabitRepository()
    val habitCooldownRepository = InMemoryHabitCooldownRepository()

    val createHabit = createHabit(habitRepository)
    val changeBalance =
        changeHabitBalance(pointsBalanceRepository, habitRepository, habitCooldownRepository)
    val observeBalance = observeHabitPointsBalance(pointsBalanceRepository)

    test("default balance should be zero") {
        observeBalance.getBalance().first() shouldBe 0
    }

    test("points should be 100 if existing balance is 100") {
        createHabit.create(walk)
        changeBalance.change(walk.id, dummyInstant)
        observeBalance.getBalance().first() shouldBe 100
    }

    test("balances updates should arrive in a sequence") {

        listOf(walk, snacking)
            .forEach { createHabit.create(it) }

        observeBalance.getBalance().test {

            awaitItem() shouldBe 0

            changeBalance.change(walk.id, dummyInstant)
            awaitItem() shouldBe 100

            changeBalance.change(snacking.id, dummyInstant)
            awaitItem() shouldBe 90

            cancelAndIgnoreRemainingEvents()
        }
    }
})
