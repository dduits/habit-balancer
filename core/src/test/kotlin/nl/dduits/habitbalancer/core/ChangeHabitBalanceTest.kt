package nl.dduits.habitbalancer.core

import io.kotest.assertions.throwables.shouldThrowExactly
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.flow.first
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant
import nl.dduits.habitbalancer.core.TestHabits.coldShower
import nl.dduits.habitbalancer.core.TestHabits.gaming
import nl.dduits.habitbalancer.core.TestHabits.meditation
import nl.dduits.habitbalancer.core.TestHabits.smoking
import nl.dduits.habitbalancer.core.TestHabits.yoga
import nl.dduits.habitbalancer.memory.InMemoryHabitCooldownRepository
import nl.dduits.habitbalancer.memory.InMemoryHabitRepository
import nl.dduits.habitbalancer.memory.InMemoryPointsBalanceRepository
import java.time.Month
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes

internal class ChangeHabitBalanceTest : FunSpec({

    val balanceRepository = InMemoryPointsBalanceRepository()
    val habitRepository = InMemoryHabitRepository()
    val habitCooldownRepository = InMemoryHabitCooldownRepository()

    val createHabit = createHabit(
        repository = habitRepository
    )

    val changeHabitBalance = changeHabitBalance(
        persistenceRepository = balanceRepository,
        habitRepository = habitRepository,
        habitCooldownRepository = habitCooldownRepository
    )

    val observeHabitBalance = observeHabitPointsBalance(
        repository = balanceRepository
    )

    test("balance should increase when habit rewards points") {
        createHabit.create(yoga)
        changeHabitBalance.change(yoga.id, dummyInstant)
        observeHabitBalance
            .getBalance()
            .first()
            .shouldBe(yoga.pointsPerHabit)
    }

    test("balance should decrease when habit subtracts points") {

        createHabit.create(coldShower)
        createHabit.create(meditation)
        createHabit.create(gaming)

        changeHabitBalance.change(coldShower.id, dummyInstant)
        changeHabitBalance.change(meditation.id, dummyInstant)
        changeHabitBalance.change(gaming.id, dummyInstant)

        observeHabitBalance
            .getBalance()
            .first()
            .shouldBe(50)
    }

    test("changeHabitBalance should error when balance would decrease below zero") {

        createHabit.create(coldShower)
        createHabit.create(smoking)

        changeHabitBalance.change(coldShower.id, dummyInstant)

        shouldThrowExactly<IllegalArgumentException> {
            changeHabitBalance.change(smoking.id, dummyInstant)
        }
    }

    test("changeHabitBalance should error when habit doesn't exist") {
        shouldThrowExactly<IllegalArgumentException> {
            changeHabitBalance.change(23, dummyInstant)
        }
    }

    test("habit should reward less when performed during cooldown") {
        val initialInstant = LocalDateTime(2022, Month.JULY, 1, 9, 30).toInstant(TimeZone.UTC)
        val laterInTheDayInstant = initialInstant + (5.hours + 30.minutes)

        createHabit.create(meditation)

        changeHabitBalance.change(meditation.id, initialInstant)
        changeHabitBalance.change(meditation.id, laterInTheDayInstant)

        observeHabitBalance
            .getBalance()
            .first()
            .shouldBe(150)
    }

    test("habit should cost more when performed twice during cooldown") {

        listOf(coldShower, smoking)
            .forEach { createHabit.create(it) }

        // Habit balance is 1500
        repeat(30) { changeHabitBalance.change(coldShower.id, dummyInstant) }

        val initialInstant = LocalDateTime(2022, Month.JULY, 13, 10, 30).toInstant(TimeZone.UTC)
        val dayLater = initialInstant + 1.days
        val dayLaterTwoHoursLater = dayLater + 2.hours

        changeHabitBalance.change(smoking.id, initialInstant)
        observeHabitBalance.getBalance().first() shouldBe 1300

        changeHabitBalance.change(smoking.id, dayLater)
        observeHabitBalance.getBalance().first() shouldBe 900

        changeHabitBalance.change(smoking.id, dayLaterTwoHoursLater)
        observeHabitBalance.getBalance().first() shouldBe 300
    }

    test("habit cost should reset after cool-down period is over") {

        listOf(yoga, smoking, gaming)
            .forEach { createHabit.create(it) }

        // Habit balance is 1000
        repeat(5) { changeHabitBalance.change(yoga.id, dummyInstant) }

        val initialInstant = LocalDateTime(2022, Month.JULY, 1, 9, 30).toInstant(TimeZone.UTC)
        val treeHoursLaterInstant = initialInstant + 3.hours
        val treeDaysLaterInstant = initialInstant + 3.days

        changeHabitBalance.change(smoking.id, initialInstant)
        observeHabitBalance.getBalance().first() shouldBe 800

        changeHabitBalance.change(gaming.id, treeHoursLaterInstant)
        observeHabitBalance.getBalance().first() shouldBe 700

        changeHabitBalance.change(smoking.id, treeDaysLaterInstant)
        observeHabitBalance.getBalance().first() shouldBe 500
    }

    test("positive habit with cooldown should reward no points during a cooldown") {

        createHabit.create(meditation)

        val instant = LocalDateTime(2023, Month.SEPTEMBER, 12, 10, 30)
            .toInstant(TimeZone.UTC)

        changeHabitBalance.change(meditation.id, instant + 15.minutes)
        changeHabitBalance.change(meditation.id, instant + 30.minutes)
        observeHabitBalance.getBalance().first() shouldBe 150

        shouldThrowExactly<IllegalArgumentException> {
            changeHabitBalance.change(meditation.id, instant + 45.minutes)
        }

        observeHabitBalance.getBalance().first() shouldBe 150
    }
})
