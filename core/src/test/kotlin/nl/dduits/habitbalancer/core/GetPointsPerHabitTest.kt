package nl.dduits.habitbalancer.core

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.shouldBe
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.Month
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant
import nl.dduits.habitbalancer.core.TestHabits.exercise
import nl.dduits.habitbalancer.core.TestHabits.gaming
import nl.dduits.habitbalancer.core.TestHabits.meditation
import nl.dduits.habitbalancer.core.TestHabits.pomodoro
import nl.dduits.habitbalancer.core.TestHabits.smoking
import nl.dduits.habitbalancer.core.TestHabits.yoga
import nl.dduits.habitbalancer.memory.InMemoryHabitCooldownRepository
import nl.dduits.habitbalancer.memory.InMemoryHabitRepository
import nl.dduits.habitbalancer.memory.InMemoryPointsBalanceRepository
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

internal class GetPointsPerHabitTest : FunSpec({

    val pointsBalanceRepository = InMemoryPointsBalanceRepository()
    val habitRepository = InMemoryHabitRepository()
    val habitCooldownRepository = InMemoryHabitCooldownRepository()

    val createHabit = createHabit(habitRepository)
    val changeHabitBalance =
        changeHabitBalance(pointsBalanceRepository, habitRepository, habitCooldownRepository)
    val calculatePoints =
        calculatePoints(pointsBalanceRepository, habitCooldownRepository)

    test("calculation should have the same values as created habit") {

        createHabit.create(pomodoro)

        calculatePoints.calculate(pomodoro, dummyInstant) shouldBe PointsCalculation(
            pointsChange = 100,
            durationForPointsChange = 25.minutes,
            disabled = false
        )
    }

    test("habit points changes with cooldown and time") {
        val weightLifting = exercise

        val initialInstant = LocalDateTime(2022, Month.JULY, 11, 12, 30).toInstant(TimeZone.UTC)

        createHabit.create(weightLifting)
        calculatePoints.calculate(weightLifting, initialInstant) shouldBe PointsCalculation(
            pointsChange = 100,
            durationForPointsChange = 15.minutes,
            disabled = false
        )

        changeHabitBalance.change(weightLifting.id, initialInstant + (0.5).days)

        calculatePoints.calculate(
            weightLifting,
            initialInstant + (0.75).days
        ) shouldBe PointsCalculation(
            pointsChange = 150,
            durationForPointsChange = 15.minutes,
            disabled = false
        )
    }

    test("habit should cost resets after cooldown period") {

        val instantInitial = LocalDateTime(2022, Month.SEPTEMBER, 5, 15, 30)
            .toInstant(TimeZone.UTC)

        listOf(exercise, yoga, meditation, gaming)
            .forEach { createHabit.create(it) }

        changeHabitBalance.change(exercise.id, instantInitial)
        changeHabitBalance.change(yoga.id, instantInitial + 2.hours)
        changeHabitBalance.change(meditation.id, instantInitial + 4.hours)

        val gamingInstant = instantInitial + 6.hours
        changeHabitBalance.change(gaming.id, gamingInstant)

        calculatePoints.calculate(gaming, gamingInstant) shouldBe PointsCalculation(
            pointsChange = -200,
            durationForPointsChange = null,
            disabled = false
        )

        calculatePoints.calculate(
            gaming,
            gamingInstant + 2.days + 1.seconds
        ) shouldBe PointsCalculation(
            pointsChange = -100,
            durationForPointsChange = null,
            disabled = false
        )
    }

    test("habit should be disabled when habit rewards 0 points") {

        createHabit.create(meditation)

        val instant = LocalDateTime(2020, Month.MARCH, 18, 11, 50)
            .toInstant(TimeZone.UTC)

        changeHabitBalance.change(meditation.id, instant)
        changeHabitBalance.change(meditation.id, instant + 5.hours)

        val calculation = calculatePoints.calculate(meditation, instant + 8.hours)

        calculation.pointsChange shouldBe 0
        calculation.disabled.shouldBeTrue()
    }

    test("habit should be disabled when points balance would become negative") {

        val instant = LocalDateTime(2020, Month.MARCH, 18, 11, 50)
            .toInstant(TimeZone.UTC)

        listOf(exercise, smoking)
            .forEach { createHabit.create(it)  }

        changeHabitBalance.change(exercise.id, instant)

        calculatePoints.calculate(smoking, instant).disabled.shouldBeTrue()
    }
})
