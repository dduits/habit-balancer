package nl.dduits.habitbalancer.core

import kotlin.time.Duration

data class Habit(
    val id: Long,
    val displayText: String,
    val pointsPerHabit: Int,
    val pointsPerHabitType: PointsPerHabit,
    val pointsModifier: PointsModifier?
) {

    sealed interface PointsPerHabit {
        object Event : PointsPerHabit
        data class Overtime(val duration: Duration) : PointsPerHabit
    }

    data class PointsModifier(val multiplier: Double, val resetPeriod: Duration)
}
