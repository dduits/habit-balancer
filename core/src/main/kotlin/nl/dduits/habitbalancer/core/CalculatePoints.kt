package nl.dduits.habitbalancer.core

import kotlinx.datetime.Instant
import kotlin.time.Duration

fun interface CalculatePoints {

    suspend fun calculate(habit: Habit, time: Instant): PointsCalculation

}

fun calculatePoints(
    balanceRepository: PointsBalanceRepository,
    cooldownRepository: HabitCooldownRepository
) = CalculatePoints { habit, time ->

    val balance = balanceRepository.getBalance()
    val cooldowns = cooldownRepository.getHabitCooldownById(habit.id)

    PointsCalculation(
        pointsChange = habit.calculatePoints(cooldowns, time),
        durationForPointsChange = determineDurationForPoints(habit),
        disabled = disabled(balance, habit.calculatePoints(cooldowns, time))
    )
}

private fun Habit.calculatePoints(
    cooldown: HabitCooldown?,
    instant: Instant
): Int = if (this.pointsModifier != null && cooldown != null) {
    HabitPointsCalculator.calculateBalanceDecreaseBasedOnCooldown(
        pointsPerHabit = pointsPerHabit,
        pointsModifier = this.pointsModifier,
        habitCooldown = cooldown,
        instant = instant
    )
} else {
    pointsPerHabit
}

private fun determineDurationForPoints(habit: Habit): Duration? =
    when (val type = habit.pointsPerHabitType) {
        Habit.PointsPerHabit.Event -> null
        is Habit.PointsPerHabit.Overtime -> type.duration
    }

private fun disabled(
    currentBalance: Int,
    pointsChange: Int
) = pointsChange == 0 || currentBalance + pointsChange < 0
