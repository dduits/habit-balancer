package nl.dduits.habitbalancer.core

import kotlinx.coroutines.flow.Flow

fun interface ObserveHabitPointsBalance {
    fun getBalance(): Flow<Int>
}

fun observeHabitPointsBalance(
    repository: PointsBalanceRepository
) = ObserveHabitPointsBalance(repository::getObservableBalance)
