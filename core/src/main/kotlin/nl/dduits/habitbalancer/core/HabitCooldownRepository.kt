package nl.dduits.habitbalancer.core

import nl.dduits.habitbalancer.core.HabitCooldown

interface HabitCooldownRepository {
    suspend fun registerHabitCooldown(habitCooldown: HabitCooldown)
    suspend fun getHabitCooldownById(id: Long): HabitCooldown?
    suspend fun getHabitCooldownsWithIds(ids: List<Long>): List<HabitCooldown>
}
