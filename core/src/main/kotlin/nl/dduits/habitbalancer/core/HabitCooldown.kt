package nl.dduits.habitbalancer.core

import kotlinx.datetime.Instant

data class HabitCooldown(
    val habitId: Long,
    val violations: Int,
    val violationInstant: Instant
)
