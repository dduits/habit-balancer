package nl.dduits.habitbalancer.core

import kotlinx.coroutines.flow.Flow
import kotlinx.datetime.Instant
import nl.dduits.habitbalancer.core.BalanceChange

interface PointsBalanceRepository {
    suspend fun getBalance(): Int
    fun getObservableBalance(): Flow<Int>
    suspend fun changeBalance(points: Int, instant: Instant)
    suspend fun getNewestBalanceChange(): BalanceChange?
    suspend fun removeNewestBalanceChange()
}
