package nl.dduits.habitbalancer.core

import kotlinx.coroutines.flow.Flow

fun interface GetHabits {
    fun get(): Flow<List<Habit>>
}

fun getHabits(
    repository: HabitRepository
) = GetHabits {
    repository.getHabits()
}
