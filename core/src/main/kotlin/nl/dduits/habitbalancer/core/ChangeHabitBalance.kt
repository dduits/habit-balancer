package nl.dduits.habitbalancer.core

import kotlinx.datetime.Instant

fun interface ChangeHabitBalance {
    suspend fun change(habitId: Long, instant: Instant)
}

fun changeHabitBalance(
    persistenceRepository: PointsBalanceRepository,
    habitRepository: HabitRepository,
    habitCooldownRepository: HabitCooldownRepository
) = ChangeHabitBalance { id, instant ->

    val habit = habitRepository.getHabitById(id)
    val currentBalance = persistenceRepository.getBalance()

    requireNotNull(habit)

    val balanceChagne: Int

    val pointsModifier = habit.pointsModifier

    if (pointsModifier != null) {

        val habitCooldown = habitCooldownRepository.getHabitCooldownById(habit.id)

        balanceChagne =
            HabitPointsCalculator.calculateBalanceDecreaseBasedOnCooldown(
                pointsPerHabit = habit.pointsPerHabit,
                pointsModifier = pointsModifier,
                habitCooldown = habitCooldown,
                instant = instant
            )

        val violations = habitCooldown?.violations?.plus(1) ?: 1
        val habitCooldown1 = HabitCooldown(habit.id, violations, instant)

        require((currentBalance + balanceChagne) >= 0)

        habitCooldownRepository.registerHabitCooldown(habitCooldown1)
    } else {
        balanceChagne = habit.pointsPerHabit
        require((currentBalance + balanceChagne) >= 0)
    }

    persistenceRepository.changeBalance(balanceChagne, instant)

}
