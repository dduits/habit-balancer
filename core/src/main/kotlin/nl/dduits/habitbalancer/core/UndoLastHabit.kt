package nl.dduits.habitbalancer.core

import kotlinx.datetime.Instant

fun interface UndoLastHabit {
    suspend fun undo(now: Instant)
}

fun undoLastHabit(
    balanceRepository: PointsBalanceRepository
) = UndoLastHabit { now ->
    val lastBalanceChange = balanceRepository.getNewestBalanceChange()

    if (lastBalanceChange != null && isOutsideOfUndoTimeframe(now, lastBalanceChange)) {
        throw IllegalStateException("Outside of undo timeframe.")
    }

    balanceRepository.removeNewestBalanceChange()
}

private fun isOutsideOfUndoTimeframe(
    now: Instant,
    lastBalanceChange: BalanceChange
): Boolean = (now - lastBalanceChange.instant).inWholeSeconds > 10
