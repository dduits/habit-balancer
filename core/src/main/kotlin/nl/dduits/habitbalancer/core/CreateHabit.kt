package nl.dduits.habitbalancer.core

import kotlinx.coroutines.flow.first
import kotlin.math.roundToInt

fun interface CreateHabit {
    suspend fun create(habit: Habit)
}

fun createHabit(
    repository: HabitRepository
): CreateHabit = CreateHabit { habit ->
    habit.validateOrThrow()
    repository.createHabit(habit)
}

private fun Habit.validateOrThrow() {
    require(displayText.trim().isNotEmpty())
    require(pointsPerHabit != 0)

    val type = pointsPerHabitType
    if (type is Habit.PointsPerHabit.Overtime) {
        require(type.duration.isPositive())
    }

    require(pointsModifier?.let { it.multiplier.roundToInt() != 0 } ?: true)
    require(pointsModifier?.resetPeriod?.isPositive() ?: true)
}