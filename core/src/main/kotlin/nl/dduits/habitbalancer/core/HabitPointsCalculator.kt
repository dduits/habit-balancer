package nl.dduits.habitbalancer.core

import kotlinx.datetime.Instant
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

internal object HabitPointsCalculator {

    fun calculateBalanceDecreaseBasedOnCooldown(
        pointsPerHabit: Int,
        pointsModifier: Habit.PointsModifier,
        habitCooldown: HabitCooldown?,
        instant: Instant
    ): Int = if (hasExistingHabitCooldown(habitCooldown)) {
        val cooldownInstant = calculateCooldownInstant(pointsModifier, habitCooldown)

        if (instant.isBefore(cooldownInstant)) {
            calculatePointsBasedOnViolations(
                pointsPerHabit = pointsPerHabit,
                pointsModifier = pointsModifier,
                violations = habitCooldown.violations
            )
        } else {
            pointsPerHabit
        }
    } else {
        pointsPerHabit
    }

    private fun calculatePointsBasedOnViolations(
        pointsPerHabit: Int,
        pointsModifier: Habit.PointsModifier,
        violations: Int
    ): Int {
        val increase = (pointsModifier.multiplier - 1) * violations * pointsPerHabit
        return (pointsPerHabit + increase).toInt()
    }

    private fun Instant.isBefore(
        cooldownInstant: Instant
    ) = this < cooldownInstant

    private fun calculateCooldownInstant(
        pointsModifier: Habit.PointsModifier,
        habitCooldown: HabitCooldown
    ): Instant {
        val cooldownDuration = pointsModifier.resetPeriod * habitCooldown.violations
        return habitCooldown.violationInstant + cooldownDuration
    }

    @OptIn(ExperimentalContracts::class)
    private fun hasExistingHabitCooldown(habitCooldown: HabitCooldown?): Boolean {
        contract {
            returns(true) implies (habitCooldown != null)
        }

        return habitCooldown != null && habitCooldown.violations > 0
    }
}
