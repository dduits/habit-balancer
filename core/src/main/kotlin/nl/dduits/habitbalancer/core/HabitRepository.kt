package nl.dduits.habitbalancer.core

import kotlinx.coroutines.flow.Flow
import nl.dduits.habitbalancer.core.Habit

interface HabitRepository {
    suspend fun createHabit(habit: Habit)
    fun getHabits(): Flow<List<Habit>>
    suspend fun getHabitById(id: Long): Habit?
}
