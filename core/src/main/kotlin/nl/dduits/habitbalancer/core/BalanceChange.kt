package nl.dduits.habitbalancer.core

import kotlinx.datetime.Instant

data class BalanceChange(val change: Int, val instant: Instant)
