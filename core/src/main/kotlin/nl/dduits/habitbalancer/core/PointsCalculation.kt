package nl.dduits.habitbalancer.core

import kotlin.time.Duration

data class PointsCalculation(
    val pointsChange: Int,
    val durationForPointsChange: Duration?,
    val disabled: Boolean
)
