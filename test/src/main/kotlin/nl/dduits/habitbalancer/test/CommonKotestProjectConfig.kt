package nl.dduits.habitbalancer.test

import io.kotest.core.config.AbstractProjectConfig
import io.kotest.core.spec.IsolationMode

class CommonKotestProjectConfig : AbstractProjectConfig() {
    override val isolationMode = IsolationMode.InstancePerTest
    override val parallelism: Int = 8
}
